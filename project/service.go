package project

import (
    "net/http"
    "os"
    "fmt"
    "log"

    "github.com/gin-gonic/gin"
    "bitbucket.org/yanple/exam/core"
    "code.google.com/p/go.net/websocket"

    "github.com/HouzuoGuo/tiedot/db"
)

type ProjectService struct {
    Debug bool
    ReceiverPort string
    StatsPort string
}


func (s *ProjectService) Run() {
    DBDir := "/tmp/MyDatabase"
    os.RemoveAll(DBDir)
    defer os.RemoveAll(DBDir)

    // (Create if not exist) open a database
    db, err := db.OpenDB(DBDir)
    if err != nil {
        panic(err)
    }

    // Create collections: Feeds and Votes
    if err := db.Create("Data"); err != nil {
        panic(err)
    }

    // Create main data object
    data := db.Use("Data")
    idMainDoc, err := data.Insert(map[string]interface{}{
        "text": "",
    })
    if err != nil {
        panic(err)
    }

    go s.RunHttp(db, idMainDoc)
    go s.RunSocket(db, idMainDoc)
    select {}
}

func (s *ProjectService) RunHttp(db *db.DB, idMainDoc int) {
    if s.Debug {
        gin.SetMode(gin.DebugMode)
    } else {
        gin.SetMode(gin.ReleaseMode)
    }
    r := gin.Default()

    var coreViews = core.CoreViews{DB: db, IdMainDoc: idMainDoc}
    r.GET("/stats", coreViews.Statistic)

    r.Run(fmt.Sprintf(":%s", s.StatsPort))
}

func (s *ProjectService) RunSocket(db *db.DB, idMainDoc int) {
    var coreViews = core.CoreViews{DB: db, IdMainDoc: idMainDoc}

    http.Handle("/", websocket.Handler(coreViews.ProcessText))

    if s.Debug {
        log.Printf("[RECEIVER-debug] Listening Websocket on :" + s.ReceiverPort)
    }

    http.ListenAndServe(fmt.Sprintf(":%s", s.ReceiverPort), nil)
}

