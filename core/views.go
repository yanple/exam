package core

import (
    "net/http"

    "fmt"
    "strconv"
    "github.com/gin-gonic/gin"
    "code.google.com/p/go.net/websocket"
    "github.com/HouzuoGuo/tiedot/db"
)


type CoreViews struct {
    DB *db.DB
    IdMainDoc int
}

func (r *CoreViews) Statistic(c *gin.Context) {
    top_count_param := c.Query("N")
    top_count, err := strconv.Atoi(top_count_param)
    if err != nil {
        // Set default value
        top_count = 5
    }

    feeds := r.DB.Use("Data")
    data, err := feeds.Read(r.IdMainDoc)
    if err != nil {
        c.JSON(http.StatusNotFound, gin.H{
            "error": "Main store not created. Please check exist document in DB.",
        })
        return
    }

    text, ok := data["text"].(string)
    if !ok {
        c.JSON(http.StatusBadRequest, gin.H{
            "error": "Can not parsed text from DB to string.",
        })
        return
    }
    if len(text) == 0 {
        c.JSON(http.StatusNotFound, gin.H{
            "error": "Text not yet received",
        })
        return
    }

    top_words, count_words := TopOfWords(text, top_count)
    top_letters := TopOfChars(text, top_count)

    top_words_param := fmt.Sprintf("top_%d_words", top_count)
    top_letters_param := fmt.Sprintf("top_%d_letters", top_count)

    c.JSON(http.StatusOK, gin.H{
        "count": count_words,
        top_words_param: top_words,
        top_letters_param: top_letters,
    })
}

func (r *CoreViews) ProcessText(ws *websocket.Conn) {
    msg := make([]byte, 512)
    n, err := ws.Read(msg)
    if err != nil {
        panic(err)
    }

    go r.UpdateText(string(msg[:n]))
}

func (r *CoreViews) UpdateText(text string) {
    feeds := r.DB.Use("Data")

    // Get old data
    data, err := feeds.Read(r.IdMainDoc)
    if err != nil {
        panic(err)
    }

    // Update text with new part
    result_text := fmt.Sprintf("%s ", data["text"]) + text
    err = feeds.Update(r.IdMainDoc, map[string]interface{}{
        "text": result_text,
    })
    if err != nil {
        panic(err)
    }
}
