package core

import (
    "net/http"
    "testing"
    "encoding/json"
    "io/ioutil"

    "code.google.com/p/go.net/websocket"
    "github.com/stretchr/testify/assert"
    "github.com/juju/errgo/errors"
)

var origin = "http://localhost/"
var receiver_url = "ws://localhost:5555/"
var statistic_url = "http://localhost:8080/"

func perror(err error) {
    if err != nil {
        panic(err)
    }
}

func sendText(text string) {
    ws, err := websocket.Dial(receiver_url, "", origin)
    perror(err)

    message := []byte(text)
    _, err = ws.Write(message)
    perror(err)
}

func TestSendText(t *testing.T) {
    sendText("Lorem Lorem lorem dolor dolor iammm ipsum ipsum rofan")
}


func TestStatsN3(t *testing.T) {
    sendText("Lorem Lorem lorem dolor dolor iammm ipsum ipsum rofan")

    assert := assert.New(t)

    resp, err := http.Get(statistic_url + "stats?N=3")
    perror(err)

    defer resp.Body.Close()
    content, err := ioutil.ReadAll(resp.Body)
    perror(err)

    var response interface{}
    err = json.Unmarshal(content, &response)
    perror(err)

    data, ok := response.(map[string]interface{})
    if !ok {
        panic(errors.New("Received incorrect json"))
    }

    // Test top 3 Words
    assert.Equal(data["top_3_words"], []interface{}{"lorem", "ipsum", "dolor"})

    // Test top 3 char
    assert.Equal(data["top_3_letters"], []interface{}{"o", "m", "r"})
}

func TestStatsN5(t *testing.T) {
    sendText("Lorem Lorem lorem dolor dolor iammm ipsum ipsum rofan")

    assert := assert.New(t)

    resp, err := http.Get(statistic_url + "stats")
    perror(err)

    defer resp.Body.Close()
    content, err := ioutil.ReadAll(resp.Body)
    perror(err)

    var response interface{}
    err = json.Unmarshal(content, &response)
    perror(err)

    data, ok := response.(map[string]interface{})
    if !ok {
        panic(errors.New("Received incorrect json"))
    }

    // Test top 5 Words
    assert.Equal(data["top_5_words"], []interface{}{"lorem", "ipsum", "dolor", "rofan", "iammm"})

    // Test top 5 char
    assert.Equal(data["top_5_letters"], []interface{}{"o", "m", "r", "l", "i"})
}