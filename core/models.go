package core

import (
    "strings"
    "sort"

    "fmt"
)

// Words
type Word struct {
    Name  string
    Count int
}

type Words []Word

func (slice Words) Len() int {
    return len(slice)
}

func (slice Words) Less(i, j int) bool {
    if slice[i].Count == slice[j].Count {
        // If count == count then sort by alphabet Name
        return slice[i].Name > slice[j].Name
    }
    return slice[i].Count > slice[j].Count
}

func (slice Words) Swap(i, j int) {
    slice[i], slice[j] = slice[j], slice[i]
}

func WordCount(s string) (Words) {
    // Strip string to [word, word]
    strs := strings.Fields(s)
    res := make(map[string]int)

    // Calculate repeated words
    for _, str := range strs {
        res[strings.ToLower(str)]++
    }

    // Convert map to array
    var arr Words
    for i, v := range res {
        arr = append(arr, Word{i, v})
    }

    // Sort by count words
    sort.Sort(arr)
    fmt.Println(arr)
    return arr
}

func TopOfWords(s string, count int) ([]string, int) {
    words := WordCount(s)

    slice_len := count
    if slice_len > len(words) {
        slice_len = len(words)
    }
    slice := words[:slice_len]

    var result []string
    for _, v := range slice {
        result = append(result, v.Name)
    }

    return result, len(words)
}

// Chars
type Char struct {
    Name  string
    Count int
}

type Chars []Char

func (slice Chars) Len() int {
    return len(slice)
}

func (slice Chars) Less(i, j int) bool {
    if slice[i].Count == slice[j].Count {
        // If count == count then sort by alphabet Name
        return slice[i].Name > slice[j].Name
    }
    return slice[i].Count > slice[j].Count
}

func (slice Chars) Swap(i, j int) {
    slice[i], slice[j] = slice[j], slice[i]
}

func CharCount(s string) Chars {
    // Split string to [char, char]. Need for remove whitespaces
    strs := strings.Fields(s)

    // Concatenate arr
    monolit := strings.Join(strs, "")

    // Split to chars
    chars := strings.Split(monolit, "")

    res := make(map[string]int)

    // Calculate repeated chars
    for _, char := range chars {
        res[strings.ToLower(char)]++
    }

    // Convert map to array
    var arr Chars
    for i, v := range res {
        arr = append(arr, Char{i, v})
    }

    // Sort by count chars
    sort.Sort(arr)

    return arr
}

func TopOfChars(s string, count int) []string {
    chars := CharCount(s)

    slice_len := count
    if slice_len > len(chars) {
        slice_len = len(chars)
    }
    slice := chars[:slice_len]

    var result []string
    for _, v := range slice {
        result = append(result, v.Name)
    }

    return result
}