package main

import (
    "os"

    "github.com/codegangsta/cli"

    "bitbucket.org/yanple/exam/project"
    "strconv"
)

func main() {
    app := cli.NewApp()
    app.Name = "Exam app"
    app.Usage = "App for parse natural text and return statistic"
    app.Version = "0.0.1"

    app.Commands = []cli.Command{
        {
            Name: "runserver",
            Usage: "Run the http server",
            Flags: []cli.Flag{
                cli.StringFlag{
                    Name: "receiver_port, rp",
                    Value: "5555",
                    Usage: "Port for socket receiver",
                },
                cli.StringFlag{
                    Name: "stats_port, sp",
                    Value: "8080",
                    Usage: "Port for web interface, which show statistic",
                },
                cli.StringFlag{
                    Name: "debug, d",
                    Value: "true",
                    Usage: "Debug mode",
                },
            },
            Action: func(c *cli.Context) {
                debug, err := strconv.ParseBool(c.String("debug"))
                if err != nil {
                    panic(err)
                }

                svc := project.ProjectService{
                    ReceiverPort: c.String("receiver_port"),
                    StatsPort: c.String("stats_port"),
                    Debug: debug,
                }

                svc.Run()
            },
        },

    }
    app.Run(os.Args)
}
