# Go Coding Test Solution

## HowTo:

    - Install requirements
    - Run with command: **go run manage.go runserver**. The command run socket and http servers.
    - Runserver receive args:
        - **receiver_port** or **rp**. *Default 5555*.
        - **stats_port** or **sp**. *Default 8080*.
        - **debug** or **d**. *Default true*.
        
    - Run test: **go test bitbucket.org/yanple/exam/core**.
     (Note: test run only with running server)
     
     
## ToDo:
    - The test should httptest for use with running server.